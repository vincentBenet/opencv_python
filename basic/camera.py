import sys
import os
import time
import cv2
import numpy
import time

dir = sys.path[0]
cap = cv2.VideoCapture(0)
cv2.namedWindow('main')   
w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    ret, img = cap.read()
    cv2.imshow('main',img)
cap.release()
cv2.destroyAllWindows()