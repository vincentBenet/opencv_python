import sys
import os
import time
import cv2
import numpy
import time
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.jpg")
img = cv2.imread(path)
res = cv2.erode(
    cv2.dilate(
        cv2.Canny(cv2.blur(img,(10,10)), 60, 60), 
        cv2.getStructuringElement(
            cv2.MORPH_RECT, 
            (
                2*8+1, 
                2*8+1
            ), 
            (
                2*8, 
                2*8
            )
        )
    ), 
    cv2.getStructuringElement(
        cv2.MORPH_RECT, 
        (
            2*8+1, 
            2*8+1
        ), 
        (
            2*8, 
            2*8
        )
    )
)

cv2.imshow('main',cv2.resize(res, (640, 480)))
cv2.waitKey(0)