import sys
import os
import time
import cv2
import numpy
import time

dir = sys.path[0]
cap = cv2.VideoCapture(0)
cv2.namedWindow('main')   
w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

def texts_add(texts):
    line = 0
    for text in texts:
        line +=1
        font = cv2.FONT_HERSHEY_SIMPLEX
        color = (255,0,0)
        thickness = 1
        lineType = cv2.LINE_AA
        size=0.001 * h
        pos = (int(0.01*w),int(h*0.03*line))
        cv2.putText(img,text,pos,font,size,color,thickness,lineType)

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    ret, img = cap.read()
    img = cv2.Canny(img, 100, 100)
    try:
        fps
    except NameError:
        fps = 30
        t0 = time.time()
    dt = max(1/60,time.time() - t0)
    fps = round(fps * 0.995 + 1 / dt * 0.005,1)
    t0 = time.time()
    texts_add([
        "fps: {0}".format(fps),
        "date {0}".format(time.strftime("%d/%m/%Y, %H:%M:%S", time.localtime(time.time())))
    ])
    
    cv2.imshow('main',img)
cap.release()
cv2.destroyAllWindows()