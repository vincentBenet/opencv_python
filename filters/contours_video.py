import sys
import os
import time
import cv2
import numpy
import random


dir = sys.path[0]
cap = cv2.VideoCapture(0)
cv2.namedWindow('main')   
w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

pixel_min_perimeter_polygone = 1000
pixel_approximation_polygone = 1
point_min_polygon = 1
pixel_filter_sound = 1
pixel_filter_form = 1

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    ret, img = cap.read()
    
    img_r = img[:,:,0]
    img_g = img[:,:,1]
    img_b = img[:,:,2]
    
    
    _, img_thres_r = cv2.threshold(img_r,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)
    _, img_thres_g = cv2.threshold(img_g,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)
    _, img_thres_b = cv2.threshold(img_b,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)
    
    img_thres = cv2.bitwise_xor(img_thres_r, img_thres_g)
    img_thres = cv2.bitwise_xor(img_thres, img_thres_b)
    
    img_blur = cv2.blur(img_thres,(pixel_filter_sound* 2 + 1,pixel_filter_sound* 2 + 1))
    _, img_thres_filtered = cv2.threshold(img_blur,0,255,cv2.THRESH_OTSU)
    
    img_erode = cv2.erode(img_thres_filtered, cv2.getStructuringElement(cv2.MORPH_RECT, (2*pixel_filter_form + 1, 2*pixel_filter_form+1), (pixel_filter_form, pixel_filter_form)))
    img_dilate = cv2.dilate(img_erode, cv2.getStructuringElement(cv2.MORPH_RECT, (2*pixel_filter_form + 1, 2*pixel_filter_form+1), (pixel_filter_form, pixel_filter_form)))
    
    v1,contours,v3 = cv2.findContours(img_dilate.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)    
    contours = sorted(contours, key = cv2.contourArea, reverse = True)
    print(numpy.shape(contours[0]))
    
    for i in range(len(contours)):
        contour = contours[i]
        peri = cv2.arcLength(contour, True)
        if peri < pixel_min_perimeter_polygone:
            continue
        if len(contour) < point_min_polygon:
            continue
        approx = cv2.approxPolyDP(contour, pixel_approximation_polygone, True)
        random.seed(i)
        color = (255*random.random(),255*random.random(),255*random.random())
        
        cv2.drawContours(img,[contour], -1, color, 3)

    cv2.imshow('main',img)

cap.release()
cv2.destroyAllWindows()