import sys
import os
import time
import cv2
import numpy
import time
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.jpg")
img = cv2.imread(path)
print(img)
t0 = time.time()
res = cv2.blur(img,(35,35))

cv2.imshow('main',cv2.resize(res, (640, 480)))
cv2.waitKey(0)