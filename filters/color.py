import sys
import os
import time
import cv2
import numpy as np
import numpy
import time
import random
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.png")
img = cv2.imread(path)

lower_blue = np.array([150,150,150])
upper_blue = np.array([255,255,255])
mask1 = cv2.inRange(img, lower_blue, upper_blue)

lower_blue = np.array([0,0,0])
upper_blue = np.array([50,50,50])
mask2 = cv2.inRange(img, lower_blue, upper_blue)

mask = cv2.bitwise_or(mask1,mask2)

res = cv2.bitwise_and(img,img, mask= mask)

cv2.imshow('res',cv2.resize(res, (640, 480)))

cv2.waitKey(0)
