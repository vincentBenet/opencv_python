import sys
import os
import time
import cv2
import numpy
import time
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.png")
img = cv2.imread(path)
print(img)
t0 = time.time()
res = cv2.Canny(cv2.blur(img,(12,12)), 70, 70)

cv2.imshow('main',cv2.resize(res, (640, 480)))
cv2.waitKey(0)