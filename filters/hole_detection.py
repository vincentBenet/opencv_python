import sys
import os
import time
import cv2
import numpy as np
import numpy
import time
import random
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.png")
img = cv2.imread(path)
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

cv2.imshow('H',cv2.resize(hsv[:,:,0], (640, 480)))
cv2.imshow('S',cv2.resize(hsv[:,:,1], (640, 480)))
cv2.imshow('V',cv2.resize(hsv[:,:,2], (640, 480)))


lower = np.array([0,0,0])
upper = np.array([20,255,255])
mask = cv2.inRange(hsv, lower, upper)

v1,contours,v3 = cv2.findContours(mask.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)    
contours = sorted(contours, key = cv2.contourArea, reverse = True)

pixel_min_perimeter_polygone = 40
pixel_approximation_polygone = 1
point_min_polygon = 1
pixel_filter_sound = 5
pixel_filter_form = 1
for i in range(len(contours)):
    contour = contours[i]
    peri = cv2.arcLength(contour, True)
    if peri < pixel_min_perimeter_polygone:
        continue
    if len(contour) < point_min_polygon:
        continue
    approx = cv2.approxPolyDP(contour, pixel_approximation_polygone, True)
    random.seed(i)
    color = (255*random.random(),255*random.random(),255*random.random())
    
    cv2.drawContours(img,[contour], -1, color, 10)


cv2.imshow('img2',cv2.resize(img, (640, 480)))

cv2.waitKey(0)
