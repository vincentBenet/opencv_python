import sys
import os
import time
import cv2
import numpy
import time
from matplotlib import pyplot as plt

dir = sys.path[0]
cv2.namedWindow('main')
path = os.path.join(dir,"..","image.jpg")
img = cv2.blur(cv2.Canny(cv2.blur(cv2.imread(path),(10,10)), 60, 60),(50,50))
# print(img)
ret,res = cv2.threshold(img,0.01,255,cv2.THRESH_BINARY)

cv2.imshow('main',cv2.resize(res, (640, 480)))
cv2.waitKey(0)