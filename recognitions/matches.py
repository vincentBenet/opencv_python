import numpy as np
import cv2
import sys
import os
import time
import time

dir = sys.path[0]
template_path = os.path.join(dir,"template.png")
template = cv2.imread(template_path)

cap = cv2.VideoCapture(0)
cv2.namedWindow('main')   
w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    ret, img = cap.read()    
    
    orb = cv2.ORB_create()
    kp1, des1 = orb.detectAndCompute(img,None)
    kp2, des2 = orb.detectAndCompute(template,None)
    
    bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)
    matches = sorted(bf.match(des1,des2),key=lambda x:x.distance)
    img = cv2.drawMatches(img,kp1,template,kp2,matches[:5],None,flags=2)
    cv2.imshow('main',img)
    
cap.release()
cv2.destroyAllWindows()