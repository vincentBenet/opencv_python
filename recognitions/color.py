import cv2
import numpy as np

cap = cv2.VideoCapture(0)

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    _, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    lower_blue = np.array([50,50,50])
    upper_blue = np.array([255,255,255])
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    res = cv2.bitwise_and(frame,frame, mask= mask)

    cv2.imshow('res',res)

cv2.destroyAllWindows()