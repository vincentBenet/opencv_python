import numpy as np
import cv2
import sys
import os
import time
import time

dir = sys.path[0]
template_path = os.path.join(dir,"..","template.jpg")
template = cv2.imread(template_path)
image_path = os.path.join(dir,"..","image.jpg")
img = cv2.imread(image_path)
cv2.namedWindow('main')   
size = np.shape(template)
w, h, p = size

res = cv2.matchTemplate(img,template,cv2.TM_SQDIFF_NORMED)
res = (1 - res)
    
loc = np.where( res == res.max())

for pt in zip(*loc[::-1]):
    cv2.rectangle(img, pt, (pt[0] + h, pt[1] + w), (255,0,0), 5)
    
cv2.imshow('main',cv2.resize(img, (640, 480)))
cv2.imshow('other',cv2.resize(res, (640, 480)))
cv2.waitKey(0)