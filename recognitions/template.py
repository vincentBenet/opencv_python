import numpy as np
import cv2
import sys
import os
import time
import time

dir = sys.path[0]
template_path = os.path.join(dir,"template.png")
template = cv2.imread(template_path)

cap = cv2.VideoCapture(0)
cv2.namedWindow('main')   
size = np.shape(template)
w, h, p = size

while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
    ret, img = cap.read()    
    
    res = cv2.matchTemplate(img,template,cv2.TM_SQDIFF_NORMED)
    res = (1 - res) ** 10
    
    loc = np.where( res == res.max())
    
    for pt in zip(*loc[::-1]):
        cv2.rectangle(img, pt, (pt[0] + h, pt[1] + w), (0,0,255), 2)


    cv2.imshow('main',img)
    cv2.imshow('other',res)
    
cap.release()
cv2.destroyAllWindows()