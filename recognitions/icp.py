import sys
import os
import time
import cv2
import numpy
import time
import random

def find_contours(img,
        pixel_filter_sound = 5,
        pixel_filter_form = 5
    ):
    img_r = img[:,:,0]
    img_g = img[:,:,1]
    img_b = img[:,:,2]

    _, img_thres_r = cv2.threshold(img_r,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)
    _, img_thres_g = cv2.threshold(img_g,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)
    _, img_thres_b = cv2.threshold(img_b,0,255,cv2.THRESH_OTSU+cv2.THRESH_BINARY_INV)

    img_thres = cv2.bitwise_xor(img_thres_r, img_thres_g)
    img_thres = cv2.bitwise_xor(img_thres, img_thres_b)

    img_blur = cv2.blur(img_thres,(pixel_filter_sound* 2 + 1,pixel_filter_sound* 2 + 1))
    _, img_thres_filtered = cv2.threshold(img_blur,0,255,cv2.THRESH_OTSU)

    img_erode = cv2.erode(img_thres_filtered, cv2.getStructuringElement(cv2.MORPH_RECT, (2*pixel_filter_form + 1, 2*pixel_filter_form+1), (pixel_filter_form, pixel_filter_form)))
    img_dilate = cv2.dilate(img_erode, cv2.getStructuringElement(cv2.MORPH_RECT, (2*pixel_filter_form + 1, 2*pixel_filter_form+1), (pixel_filter_form, pixel_filter_form)))

    v1,contours,v3 = cv2.findContours(img_dilate.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)
    return numpy.array(sorted(contours, key = cv2.contourArea, reverse = True))
def fusion_contours(contours):
    n_contours = len(contours)
    L = []
    for i in range(n_contours):
        contour = contours[i]
        n_point = len(contour)
        for j in range(n_point):
            x = contour[j,0,0]
            y = contour[j,0,1]
            L.append([x,y])
    out = numpy.array(L)
    return numpy.array(out).T
def trace_contours(img,contours,
        pixel_min_perimeter_polygone = 0,
        pixel_approximation_polygone = 0.1,
        point_min_polygon = 0
    ):
    for i in range(len(contours)):
        contour = contours[i]
        peri = cv2.arcLength(contour, True)
        if peri < pixel_min_perimeter_polygone:
            continue
        if len(contour) < point_min_polygon:
            continue
        approx = cv2.approxPolyDP(contour, pixel_approximation_polygone, True)
        random.seed(i)
        color = (255*random.random(),255*random.random(),255*random.random())
        cv2.drawContours(img,[approx], -1, color, 3)
    return img
def calcul_fps(img):
    global t_frame
    dt = max(time.time() - t_frame,1/60)
    t_frame = time.time()
    fps = round(1 / dt,2)
    img = texts_add(img,[
        "fps: {0}".format(fps),
        "date {0}".format(time.strftime("%d/%m/%Y, %H:%M:%S", time.localtime(time.time())))
    ])
    return img
def icp(a, b,
        max_time = 1
    ):
    import cv2
    import numpy
    import copy
    import pylab
    import time
    import sys
    import sklearn.neighbors
    import scipy.optimize
    
    def res(p,src,dst):
        T = numpy.matrix([[numpy.cos(p[2]),-numpy.sin(p[2]),p[0]],
        [numpy.sin(p[2]), numpy.cos(p[2]),p[1]],
        [0 ,0 ,1 ]])
        n = numpy.size(src,0)
        xt = numpy.ones([n,3])
        xt[:,:-1] = src
        xt = (xt*T.T).A
        d = numpy.zeros(numpy.shape(src))
        d[:,0] = xt[:,0]-dst[:,0]
        d[:,1] = xt[:,1]-dst[:,1]
        r = numpy.sum(numpy.square(d[:,0])+numpy.square(d[:,1]))
        return r

    def jac(p,src,dst):
        T = numpy.matrix([[numpy.cos(p[2]),-numpy.sin(p[2]),p[0]],
        [numpy.sin(p[2]), numpy.cos(p[2]),p[1]],
        [0 ,0 ,1 ]])
        n = numpy.size(src,0)
        xt = numpy.ones([n,3])
        xt[:,:-1] = src
        xt = (xt*T.T).A
        d = numpy.zeros(numpy.shape(src))
        d[:,0] = xt[:,0]-dst[:,0]
        d[:,1] = xt[:,1]-dst[:,1]
        dUdth_R = numpy.matrix([[-numpy.sin(p[2]),-numpy.cos(p[2])],
                            [ numpy.cos(p[2]),-numpy.sin(p[2])]])
        dUdth = (src*dUdth_R.T).A
        g = numpy.array([  numpy.sum(2*d[:,0]),
                        numpy.sum(2*d[:,1]),
                        numpy.sum(2*(d[:,0]*dUdth[:,0]+d[:,1]*dUdth[:,1])) ])
        return g
    def hess(p,src,dst):
        n = numpy.size(src,0)
        T = numpy.matrix([[numpy.cos(p[2]),-numpy.sin(p[2]),p[0]],
        [numpy.sin(p[2]), numpy.cos(p[2]),p[1]],
        [0 ,0 ,1 ]])
        n = numpy.size(src,0)
        xt = numpy.ones([n,3])
        xt[:,:-1] = src
        xt = (xt*T.T).A
        d = numpy.zeros(numpy.shape(src))
        d[:,0] = xt[:,0]-dst[:,0]
        d[:,1] = xt[:,1]-dst[:,1]
        dUdth_R = numpy.matrix([[-numpy.sin(p[2]),-numpy.cos(p[2])],[numpy.cos(p[2]),-numpy.sin(p[2])]])
        dUdth = (src*dUdth_R.T).A
        H = numpy.zeros([3,3])
        H[0,0] = n*2
        H[0,2] = numpy.sum(2*dUdth[:,0])
        H[1,1] = n*2
        H[1,2] = numpy.sum(2*dUdth[:,1])
        H[2,0] = H[0,2]
        H[2,1] = H[1,2]
        d2Ud2th_R = numpy.matrix([[-numpy.cos(p[2]), numpy.sin(p[2])],[-numpy.sin(p[2]),-numpy.cos(p[2])]])
        d2Ud2th = (src*d2Ud2th_R.T).A
        H[2,2] = numpy.sum(2*(numpy.square(dUdth[:,0])+numpy.square(dUdth[:,1]) + d[:,0]*d2Ud2th[:,0]+d[:,0]*d2Ud2th[:,0]))
        return H
    t0 = time.time()
    init_pose = (0,0,0)
    src = numpy.array([a.T], copy=True).astype(numpy.float32)
    dst = numpy.array([b.T], copy=True).astype(numpy.float32)
    Tr = numpy.array([[numpy.cos(init_pose[2]),-numpy.sin(init_pose[2]),init_pose[0]],
                   [numpy.sin(init_pose[2]), numpy.cos(init_pose[2]),init_pose[1]],
                   [0,                    0,                   1          ]])
    src = cv2.transform(src, Tr[0:2])
    p_opt = numpy.array(init_pose)
    T_opt = numpy.array([])
    error_max = sys.maxsize
    first = False
    while not(first and time.time() - t0 > max_time):
        distances, indices = sklearn.neighbors.NearestNeighbors(n_neighbors=1, algorithm='auto',p = 3).fit(dst[0]).kneighbors(src[0])
        p = scipy.optimize.minimize(res,[0,0,0],args=(src[0],dst[0, indices.T][0]),method='Newton-CG',jac=jac,hess=hess).x
        T  = numpy.array([[numpy.cos(p[2]),-numpy.sin(p[2]),p[0]],[numpy.sin(p[2]), numpy.cos(p[2]),p[1]]])
        p_opt[:2]  = (p_opt[:2]*numpy.matrix(T[:2,:2]).T).A       
        p_opt[0] += p[0]
        p_opt[1] += p[1]
        p_opt[2] += p[2]
        src = cv2.transform(src, T)
        Tr = (numpy.matrix(numpy.vstack((T,[0,0,1])))*numpy.matrix(Tr)).A
        error = res([0,0,0],src[0],dst[0, indices.T][0])
        
        if error < error_max:
            error_max = error
            first = True
            T_opt = Tr
        
    p_opt[2] = p_opt[2] % (2*numpy.pi)
    return T_opt, error_max
def texts_add(img,texts):
    line = 0
    for text in texts:
        line +=1
        font = cv2.FONT_HERSHEY_SIMPLEX
        color = (255,0,0)
        thickness = 1
        lineType = cv2.LINE_AA
        size=0.001 * h
        pos = (int(0.01*w),int(h*0.03*line))
        cv2.putText(img,text,pos,font,size,color,thickness,lineType)
    return img
def find_template(contour_template,contours):
    print("numpy.shape(contour_template)",numpy.shape(contour_template))
    error_opt = sys.maxsize
    for contour in contours:
        print("numpy.shape(contour)",numpy.shape(contour))
        contour = numpy.squeeze(contour,axis=1).shape
        print("numpy.shape(contour)",numpy.shape(contour))
        T, error = icp(contour,contour_template)
        if error < error_opt:
            error_opt = error
            T_opt = T
    return T_opt, error_opt
    
def main():
    while(cap.isOpened() and not(cv2.waitKey(1) & 0xff == ord('a'))):
        ret, img = cap.read()
        contours = find_contours(img)
        T, error = find_template(contour_template,contours)
        dx = int(abs(T[0,2]))
        dy = int(abs(T[1,2]))
        rotation = numpy.arcsin(T[0,1]) * 360 / 2 / numpy.pi
        print("T",T)
        print("error",error)
        print("rotation°",rotation)
        print("dx",dx)
        print("dy",dy)
        
        img = trace_contours(img,contours)
        img = trace_contours(img,contours_template)
        # img = cv2.rectangle(img, (dx,dy), (dx+w_template, dy+h_template), (255, 0, 0) , 2) 
        img = calcul_fps(img)
        cv2.imshow('main',img)
        
        
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    t_frame = time.time()
    dir = sys.path[0]
    template_path = os.path.join(dir,"template.png")
    template = cv2.imread(template_path)
    contours_template = find_contours(template)
    contour_template = fusion_contours(contours_template)
    cap = cv2.VideoCapture(0)
    cv2.namedWindow('main')   
    w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    size_template = numpy.shape(template)
    w_template = size_template[0]
    h_template = size_template[1]
    
    main()